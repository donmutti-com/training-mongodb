package ru.digisign.stuff.mongodb;

import com.mongodb.*;
import com.mongodb.client.*;
import org.bson.Document;

import javax.xml.parsers.DocumentBuilder;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author dpetuhov
 */
public class Application {

    public static void main(String args[]) {
        // Connect to Mongo
        MongoClient mongo = new MongoClient("localhost", 27017);

        // Get list of databases
        MongoIterable<String> dbNames = mongo.listDatabaseNames();
        for (String dbName: dbNames) {
            System.out.println(dbName);
        }


        // test: Get list of collections
        MongoDatabase db = mongo.getDatabase("test");
        MongoIterable<String> collectionNames = db.listCollectionNames();
        for (String collectionName: collectionNames) {
            System.out.println(collectionName);
        }

        // test: users: Get a collection
        MongoCollection<Document> users = db.getCollection("users");

        // test: users: Insert a document
        Document user = new Document();
        user.put("name", "Иванов");
        user.put("job", "Developer");
        user.put("grade", "Middle");
        users.insertOne(user);


        // test: users: Get all documents
        FindIterable<Document> userList = users.find();
        for (Document u: userList) {
            System.out.println(u);
        }

    }
}
